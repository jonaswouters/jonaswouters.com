---
title: "About Jonas Wouters"
type: about
---

Professional programmer since 2001, freelance developer since 2011, husband since 2012 and father since 2014.
I don't know what else to tell you.

You can find me on [Github](https://github.com/jonaswouters), [Facebook](https://facebook.com/jonaswouters.be), [LinkedIn](https://linkedin.com/in/jonaswouters) and [Twitter](https://twitter.com/jonaswouters).