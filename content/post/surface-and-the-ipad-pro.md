---
date: "2015-10-10T22:43:40+02:00"
title: "Surface and the iPad Pro"
description: "If only I could buy all the devices! Microsoft is changing and I'll be keeping an eye on them more often."
tags: [hardware]
header_image: "/images/post/ipadpro.png"
header_image_alt: "iPad Pro"
---
For the first time in my life I watched a Microsoft event live. I did not expect much from it except for the surface 4, which I've been waiting for. It was an amazing event. I even got excited about their lumia phones and I even consider buying it over the iPhone 6s.

Then they announced the Surface Book. “I want one” was the first thought that popped into my head. Later I realised that the screen only holds a charge for 2 hours, that was kind of a disappointment. The Surface Pro 4 is probably all I need, but I won’t be able to use it for games :(.

![Surface Pro 4](/images/post/surfacepro4.jpg)

Still having second thoughts about buying one probably means it’s not ready for me yet. They are coming closer and closer and I’m even a bit excited about Windows 10. It’s not as good as OSX is for me, but it might be next year.

The past few Apple events did nothing for me. The last thing I was excited about was the Swift language announcement. I might have been expecting too much from Apple the last couple of years, but I found all the announcements disappointing. Especially the “Macbook” announcement, I was waiting for an Macbook air with a retina display, and all we got was a slow “Macbook” with a crappy keyboard. I love the form factor, but I can’t really do anything with it. If it was 500 euros I’d probably buy it, but at this price point it’s not for me.

Enter the iPad Pro. At first I was disappointed about missing 3D Touch. To me, 3D Tourch seems like an excellent feature to have on a “pro” tablet. I can’t really make up a reason to buy one yet, until I start thinking about all the apps I find a perfect fit for this device (and the Microsoft Surface) and I get all excited. The iPad Pro would fit better in my ecosystem of OSX and iOS devices and the price point is easier to justify then a decent configured surface pro. I know they are not the same kind of device, but at least I don't have to use Windows.

![iPad Pro](/images/post/ipadpro.png)

I am undecided and I'll probably get the iPad Pro or maybe even the Surface Pro 4 at some point when I'm feeling impulsive.